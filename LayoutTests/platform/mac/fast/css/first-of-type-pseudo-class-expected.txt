layer at (0,0) size 785x2882
  RenderView at (0,0) size 785x600
layer at (0,0) size 785x2882
  RenderBlock {HTML} at (0,0) size 785x2882
    RenderBody {BODY} at (8,16) size 769x2818 [bgcolor=#FFFFFF]
      RenderBlock {P} at (0,0) size 769x18
        RenderText {#text} at (0,0) size 168x17
          text run at (0,0) width 168: "This page is part of the "
        RenderInline {A} at (0,0) size 73x17 [color=#0000EE]
          RenderText {#text} at (167,0) size 73x17
            text run at (167,0) width 73: "CSS3.info"
        RenderText {#text} at (239,0) size 5x17
          text run at (239,0) width 5: " "
        RenderInline {A} at (0,0) size 133x17 [color=#0000EE]
          RenderText {#text} at (243,0) size 133x17
            text run at (243,0) width 133: "CSS selectors test"
        RenderText {#text} at (375,0) size 136x17
          text run at (375,0) width 136: ". See more info on "
        RenderInline {A} at (0,0) size 111x17 [color=#0000EE]
          RenderText {#text} at (510,0) size 111x17
            text run at (510,0) width 111: "CSS3 selectors"
        RenderText {#text} at (620,0) size 6x17
          text run at (620,0) width 6: "."
      RenderBlock {DIV} at (0,34) size 769x0 [bgcolor=#009900]
      RenderBlock {OL} at (0,34) size 769x2784
        RenderListItem {LI} at (40,0) size 616x210 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#990000]
            RenderBlock {DIV} at (0,0) size 584x24 [bgcolor=#009900]
          RenderBlock {PRE} at (16,53) size 584x73 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,4) size 18x18: "1"
            RenderText {#text} at (6,6) size 274x61
              text run at (6,6) width 149: "div:first-of-type {"
              text run at (154,6) width 1: " "
              text run at (6,21) width 9: "}"
              text run at (14,21) width 1: " "
              text run at (6,36) width 1: " "
              text run at (6,51) width 274: "<div>Does this element match?</div>"
          RenderBlock {P} at (16,142) size 584x36
            RenderText {#text} at (0,0) size 555x35
              text run at (0,0) width 555: "The CSS selector should match the marked div element, because it is the only"
              text run at (0,18) width 139: "element of this type"
        RenderListItem {LI} at (40,258) size 616x225 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#990000]
            RenderBlock {DIV} at (0,0) size 584x24 [bgcolor=#009900]
            RenderBlock {DIV} at (0,24) size 584x0
          RenderBlock {PRE} at (16,53) size 584x88 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,4) size 18x18: "2"
            RenderText {#text} at (6,6) size 274x76
              text run at (6,6) width 149: "div:first-of-type {"
              text run at (154,6) width 1: " "
              text run at (6,21) width 9: "}"
              text run at (14,21) width 1: " "
              text run at (6,36) width 1: " "
              text run at (6,51) width 274: "<div>Does this element match?</div>"
              text run at (279,51) width 1: " "
              text run at (6,66) width 87: "<div></div>"
          RenderBlock {P} at (16,157) size 584x36
            RenderText {#text} at (0,0) size 551x35
              text run at (0,0) width 551: "The CSS selector should match the marked div element, because it is the first"
              text run at (0,18) width 139: "element of this type"
        RenderListItem {LI} at (40,531) size 616x225 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#990000]
            RenderBlock {BLOCKQUOTE} at (0,0) size 584x0 [bgcolor=#009900]
            RenderBlock {DIV} at (0,0) size 584x24 [bgcolor=#009900]
          RenderBlock {PRE} at (16,53) size 584x88 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,4) size 18x18: "3"
            RenderText {#text} at (6,6) size 274x76
              text run at (6,6) width 149: "div:first-of-type {"
              text run at (154,6) width 1: " "
              text run at (6,21) width 9: "}"
              text run at (14,21) width 1: " "
              text run at (6,36) width 1: " "
              text run at (6,51) width 196: "<blockquote></blockquote>"
              text run at (201,51) width 1: " "
              text run at (6,66) width 274: "<div>Does this element match?</div>"
          RenderBlock {P} at (16,157) size 584x36
            RenderText {#text} at (0,0) size 551x35
              text run at (0,0) width 551: "The CSS selector should match the marked div element, because it is the first"
              text run at (0,18) width 139: "element of this type"
        RenderListItem {LI} at (40,804) size 616x255 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#990000]
            RenderBlock {DIV} at (0,0) size 584x0 [bgcolor=#009900]
            RenderBlock {BLOCKQUOTE} at (0,0) size 584x24 [bgcolor=#009900]
              RenderBlock {DIV} at (0,0) size 584x24
          RenderBlock {PRE} at (16,53) size 584x118 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,4) size 18x18: "4"
            RenderText {#text} at (6,6) size 297x106
              text run at (6,6) width 149: "div:first-of-type {"
              text run at (154,6) width 1: " "
              text run at (6,21) width 9: "}"
              text run at (14,21) width 1: " "
              text run at (6,36) width 1: " "
              text run at (6,51) width 87: "<div></div>"
              text run at (92,51) width 1: " "
              text run at (6,66) width 95: "<blockquote>"
              text run at (100,66) width 1: " "
              text run at (6,81) width 297: "   <div>Does this element match?</div>"
              text run at (302,81) width 1: " "
              text run at (6,96) width 102: "</blockquote>"
          RenderBlock {P} at (16,187) size 584x36
            RenderText {#text} at (0,0) size 551x35
              text run at (0,0) width 551: "The CSS selector should match the marked div element, because it is the first"
              text run at (0,18) width 233: "element of this type in this scope"
        RenderListItem {LI} at (40,1107) size 616x240 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#990000]
            RenderBlock {DIV} at (0,0) size 584x24 [bgcolor=#009900]
              RenderBlock {DIV} at (0,0) size 584x24
          RenderBlock {PRE} at (16,53) size 584x103 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,4) size 18x18: "5"
            RenderText {#text} at (6,6) size 297x91
              text run at (6,6) width 149: "div:first-of-type {"
              text run at (154,6) width 1: " "
              text run at (6,21) width 9: "}"
              text run at (14,21) width 1: " "
              text run at (6,36) width 1: " "
              text run at (6,51) width 40: "<div>"
              text run at (45,51) width 1: " "
              text run at (6,66) width 297: "   <div>Does this element match?</div>"
              text run at (302,66) width 1: " "
              text run at (6,81) width 48: "</div>"
          RenderBlock {P} at (16,172) size 584x36
            RenderText {#text} at (0,0) size 551x35
              text run at (0,0) width 551: "The CSS selector should match the marked div element, because it is the first"
              text run at (0,18) width 284: "element of this type in the current scope"
        RenderListItem {LI} at (40,1395) size 616x255 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#990000]
            RenderBlock {BLOCKQUOTE} at (0,0) size 584x0 [bgcolor=#009900]
              RenderBlock {DIV} at (0,0) size 584x0
            RenderBlock {DIV} at (0,0) size 584x24 [bgcolor=#009900]
          RenderBlock {PRE} at (16,53) size 584x118 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,4) size 18x18: "6"
            RenderText {#text} at (6,6) size 274x106
              text run at (6,6) width 149: "div:first-of-type {"
              text run at (154,6) width 1: " "
              text run at (6,21) width 9: "}"
              text run at (14,21) width 1: " "
              text run at (6,36) width 1: " "
              text run at (6,51) width 95: "<blockquote>"
              text run at (100,51) width 1: " "
              text run at (6,66) width 110: "   <div></div>"
              text run at (115,66) width 1: " "
              text run at (6,81) width 102: "</blockquote>"
              text run at (107,81) width 1: " "
              text run at (6,96) width 274: "<div>Does this element match?</div>"
          RenderBlock {P} at (16,187) size 584x36
            RenderText {#text} at (0,0) size 551x35
              text run at (0,0) width 551: "The CSS selector should match the marked div element, because it is the first"
              text run at (0,18) width 284: "element of this type in the current scope"
        RenderListItem {LI} at (40,1698) size 616x225 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#009900]
            RenderBlock {DIV} at (0,0) size 584x0 [bgcolor=#990000]
            RenderBlock {DIV} at (0,0) size 584x24
          RenderBlock {PRE} at (16,53) size 584x88 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,4) size 18x18: "7"
            RenderText {#text} at (6,6) size 274x76
              text run at (6,6) width 149: "div:first-of-type {"
              text run at (154,6) width 1: " "
              text run at (6,21) width 9: "}"
              text run at (14,21) width 1: " "
              text run at (6,36) width 1: " "
              text run at (6,51) width 87: "<div></div>"
              text run at (92,51) width 1: " "
              text run at (6,66) width 274: "<div>Does this element match?</div>"
          RenderBlock {P} at (16,157) size 584x36
            RenderText {#text} at (0,0) size 547x35
              text run at (0,0) width 547: "The CSS selector should not match the marked div element, because it is the"
              text run at (0,18) width 195: "second element of this type"
        RenderListItem {LI} at (40,1971) size 616x225 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#009900]
            RenderBlock {DIV} at (0,0) size 584x0 [bgcolor=#990000]
            RenderBlock {DIV} at (0,0) size 584x24
          RenderBlock {PRE} at (16,53) size 584x88 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,4) size 18x18: "8"
            RenderText {#text} at (6,6) size 274x76
              text run at (6,6) width 149: "div:first-of-type {"
              text run at (154,6) width 1: " "
              text run at (6,21) width 9: "}"
              text run at (14,21) width 1: " "
              text run at (6,36) width 1: " "
              text run at (6,51) width 87: "<DIV></DIV>"
              text run at (92,51) width 1: " "
              text run at (6,66) width 274: "<div>Does this element match?</div>"
          RenderBlock {P} at (16,157) size 584x36
            RenderText {#text} at (0,0) size 547x35
              text run at (0,0) width 547: "The CSS selector should not match the marked div element, because it is the"
              text run at (0,18) width 195: "second element of this type"
        RenderListItem {LI} at (40,2244) size 616x255 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#990000]
            RenderBlock {DIV} at (0,0) size 584x24 [bgcolor=#009900]
            RenderBlock {DIV} at (0,24) size 584x0
          RenderBlock {PRE} at (16,53) size 584x118 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,4) size 18x18: "9"
            RenderText {#text} at (6,6) size 508x106
              text run at (6,6) width 149: "div:first-of-type {"
              text run at (154,6) width 1: " "
              text run at (6,21) width 9: "}"
              text run at (14,21) width 1: " "
              text run at (6,36) width 1: " "
              text run at (6,51) width 227: "<div id='insertBefore'></div>"
              text run at (232,51) width 1: " "
              text run at (6,66) width 1: " "
              text run at (6,81) width 383: "var ib = document.getElementById('insertBefore');"
              text run at (388,81) width 1: " "
              text run at (6,96) width 508: "ib.parentElement.insertBefore(document.createElement(\"div\"), ib);"
          RenderBlock {P} at (16,187) size 584x36
            RenderText {#text} at (0,0) size 571x35
              text run at (0,0) width 571: "The CSS selector should match the div element that is inserted by the Javascript"
              text run at (0,18) width 40: "code."
        RenderListItem {LI} at (40,2547) size 616x237 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#009900]
            RenderBlock {DIV} at (0,0) size 584x0 [bgcolor=#990000]
            RenderBlock {DIV} at (0,0) size 584x24
          RenderBlock {PRE} at (16,53) size 584x118 [bgcolor=#FFFFFF]
            RenderListMarker at (-47,4) size 27x18: "10"
            RenderText {#text} at (6,6) size 508x106
              text run at (6,6) width 149: "div:first-of-type {"
              text run at (154,6) width 1: " "
              text run at (6,21) width 9: "}"
              text run at (14,21) width 1: " "
              text run at (6,36) width 1: " "
              text run at (6,51) width 227: "<div id='insertBefore'></div>"
              text run at (232,51) width 1: " "
              text run at (6,66) width 1: " "
              text run at (6,81) width 383: "var ib = document.getElementById('insertBefore');"
              text run at (388,81) width 1: " "
              text run at (6,96) width 508: "ib.parentElement.insertBefore(document.createElement(\"div\"), ib);"
          RenderBlock {P} at (16,187) size 584x18
            RenderText {#text} at (0,0) size 535x17
              text run at (0,0) width 535: "The original div element should not be a match for the :first-of-type selector."

CONSOLE MESSAGE: Cross origin requests are only supported for HTTP.
CONSOLE MESSAGE: XMLHttpRequest cannot load mailto:test@example.org due to access control checks.
CONSOLE MESSAGE: Cross origin requests are only supported for HTTP.
CONSOLE MESSAGE: XMLHttpRequest cannot load tel:+31600000000 due to access control checks.
CONSOLE MESSAGE: Origin http://web-platform.test:8800 is not allowed by Access-Control-Allow-Origin. Status code: 200
CONSOLE MESSAGE: XMLHttpRequest cannot load http://www1.web-platform.test:8800/ due to access control checks.
CONSOLE MESSAGE: Cross origin requests are only supported for HTTP.
CONSOLE MESSAGE: XMLHttpRequest cannot load javascript:alert('FAIL') due to access control checks.
CONSOLE MESSAGE: Origin http://web-platform.test:8800 is not allowed by Access-Control-Allow-Origin. Status code: 404
CONSOLE MESSAGE: XMLHttpRequest cannot load http://www1.web-platform.test:8800/folder.txt due to access control checks.
CONSOLE MESSAGE: Cross origin requests are only supported for HTTP.
CONSOLE MESSAGE: XMLHttpRequest cannot load about:blank due to access control checks.
CONSOLE MESSAGE: Cross origin requests are only supported for HTTP.
CONSOLE MESSAGE: XMLHttpRequest cannot load blob:bogusidentifier due to access control checks.

PASS XMLHttpRequest: send() - non same-origin (mailto:test@example.org)
PASS XMLHttpRequest: send() - non same-origin (tel:+31600000000)
PASS XMLHttpRequest: send() - non same-origin (http://www1.web-platform.test:8800)
PASS XMLHttpRequest: send() - non same-origin (javascript:alert('FAIL'))
PASS XMLHttpRequest: send() - non same-origin (folder.txt)
PASS XMLHttpRequest: send() - non same-origin (about:blank)
PASS XMLHttpRequest: send() - non same-origin (blob:bogusidentifier)


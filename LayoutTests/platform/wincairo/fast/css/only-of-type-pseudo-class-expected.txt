layer at (0,0) size 785x1518
  RenderView at (0,0) size 785x600
layer at (0,0) size 785x1518
  RenderBlock {HTML} at (0,0) size 785x1518
    RenderBody {BODY} at (8,16) size 769x1454 [bgcolor=#FFFFFF]
      RenderBlock {P} at (0,0) size 769x19
        RenderText {#text} at (0,0) size 162x18
          text run at (0,0) width 162: "This page is part of the "
        RenderInline {A} at (0,0) size 72x18 [color=#0000EE]
          RenderText {#text} at (162,0) size 72x18
            text run at (162,0) width 72: "CSS3.info"
        RenderText {#text} at (234,0) size 4x18
          text run at (234,0) width 4: " "
        RenderInline {A} at (0,0) size 130x18 [color=#0000EE]
          RenderText {#text} at (238,0) size 130x18
            text run at (238,0) width 130: "CSS selectors test"
        RenderText {#text} at (368,0) size 131x18
          text run at (368,0) width 131: ". See more info on "
        RenderInline {A} at (0,0) size 110x18 [color=#0000EE]
          RenderText {#text} at (499,0) size 110x18
            text run at (499,0) width 110: "CSS3 selectors"
        RenderText {#text} at (609,0) size 4x18
          text run at (609,0) width 4: "."
      RenderBlock {DIV} at (0,35) size 769x0 [bgcolor=#009900]
      RenderBlock {OL} at (0,35) size 769x1419
        RenderListItem {LI} at (40,0) size 616x216 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#990000]
            RenderBlock {DIV} at (0,0) size 584x24 [bgcolor=#009900]
          RenderBlock {PRE} at (16,53) size 584x77 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,3) size 17x19: "1"
            RenderText {#text} at (6,6) size 281x65
              text run at (6,6) width 145: "div:only-of-type {"
              text run at (150,6) width 1: " "
              text run at (6,22) width 9: "}"
              text run at (14,22) width 1: " "
              text run at (6,38) width 1: " "
              text run at (6,54) width 281: "<div>Does this element match?</div>"
          RenderBlock {P} at (16,146) size 584x38
            RenderText {#text} at (0,0) size 535x37
              text run at (0,0) width 535: "The CSS selector should match the marked div element, because it is the only"
              text run at (0,19) width 133: "element of this type"
        RenderListItem {LI} at (40,264) size 616x232 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#990000]
            RenderBlock {DIV} at (0,0) size 584x24 [bgcolor=#009900]
            RenderBlock {BLOCKQUOTE} at (0,24) size 584x0 [bgcolor=#009900]
          RenderBlock {PRE} at (16,53) size 584x93 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,3) size 17x19: "2"
            RenderText {#text} at (6,6) size 281x81
              text run at (6,6) width 145: "div:only-of-type {"
              text run at (150,6) width 1: " "
              text run at (6,22) width 9: "}"
              text run at (14,22) width 1: " "
              text run at (6,38) width 1: " "
              text run at (6,54) width 281: "<div>Does this element match?</div>"
              text run at (286,54) width 1: " "
              text run at (6,70) width 201: "<blockquote></blockquote>"
          RenderBlock {P} at (16,162) size 584x38
            RenderText {#text} at (0,0) size 535x37
              text run at (0,0) width 535: "The CSS selector should match the marked div element, because it is the only"
              text run at (0,19) width 133: "element of this type"
        RenderListItem {LI} at (40,544) size 616x264 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#990000]
            RenderBlock {DIV} at (0,0) size 584x24 [bgcolor=#009900]
            RenderBlock {BLOCKQUOTE} at (0,24) size 584x0 [bgcolor=#009900]
              RenderBlock {DIV} at (0,0) size 584x0
          RenderBlock {PRE} at (16,53) size 584x125 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,3) size 17x19: "3"
            RenderText {#text} at (6,6) size 281x113
              text run at (6,6) width 145: "div:only-of-type {"
              text run at (150,6) width 1: " "
              text run at (6,22) width 9: "}"
              text run at (14,22) width 1: " "
              text run at (6,38) width 1: " "
              text run at (6,54) width 281: "<div>Does this element match?</div>"
              text run at (286,54) width 1: " "
              text run at (6,70) width 97: "<blockquote>"
              text run at (102,70) width 1: " "
              text run at (6,86) width 113: "   <div></div>"
              text run at (118,86) width 1: " "
              text run at (6,102) width 105: "</blockquote>"
          RenderBlock {P} at (16,194) size 584x38
            RenderText {#text} at (0,0) size 535x37
              text run at (0,0) width 535: "The CSS selector should match the marked div element, because it is the only"
              text run at (0,19) width 224: "element of this type in this scope"
        RenderListItem {LI} at (40,856) size 616x232 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#009900]
            RenderBlock {DIV} at (0,0) size 584x24
            RenderBlock {DIV} at (0,24) size 584x0
          RenderBlock {PRE} at (16,53) size 584x93 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,3) size 17x19: "4"
            RenderText {#text} at (6,6) size 281x81
              text run at (6,6) width 145: "div:only-of-type {"
              text run at (150,6) width 1: " "
              text run at (6,22) width 9: "}"
              text run at (14,22) width 1: " "
              text run at (6,38) width 1: " "
              text run at (6,54) width 281: "<div>Does this element match?</div>"
              text run at (286,54) width 1: " "
              text run at (6,70) width 89: "<div></div>"
          RenderBlock {P} at (16,162) size 584x38
            RenderText {#text} at (0,0) size 554x37
              text run at (0,0) width 554: "The CSS selector should not match the marked div element, because it is not the"
              text run at (0,19) width 164: "only element of this type"
        RenderListItem {LI} at (40,1136) size 616x283 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 584x24 [bgcolor=#009900]
            RenderBlock {DIV} at (0,0) size 584x24
            RenderBlock {DIV} at (0,24) size 584x0
          RenderBlock {PRE} at (16,53) size 584x125 [bgcolor=#FFFFFF]
            RenderListMarker at (-38,3) size 17x19: "5"
            RenderText {#text} at (6,6) size 481x113
              text run at (6,6) width 145: "div:only-of-type {"
              text run at (150,6) width 1: " "
              text run at (6,22) width 9: "}"
              text run at (14,22) width 1: " "
              text run at (6,38) width 1: " "
              text run at (6,54) width 225: "<div id='appendChild'></div>"
              text run at (230,54) width 1: " "
              text run at (6,70) width 1: " "
              text run at (6,86) width 385: "var ib = document.getElementById('appendChild');"
              text run at (390,86) width 1: " "
              text run at (6,102) width 481: "ib.parentElement.appendChild(document.createElement(\"div\"));"
          RenderBlock {P} at (16,194) size 584x57
            RenderText {#text} at (0,0) size 583x56
              text run at (0,0) width 506: "The CSS selector should not match the original div element, because it is "
              text run at (506,0) width 77: "not the only"
              text run at (0,19) width 466: "of its type anymore after another child with the same type is append "
              text run at (466,19) width 116: "by the Javascript"
              text run at (0,38) width 39: "code."
